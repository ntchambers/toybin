#!/usr/bin/env ruby

def die(msg)
  STDERR.puts(msg)
  exit(1)
end

die("no expression provided") if ARGV.empty?

ops = {
  '+': proc { |lhs, rhs| lhs + rhs },
  '-': proc { |lhs, rhs| lhs - rhs },
  '*': proc { |lhs, rhs| lhs * rhs },
  '/': proc { |lhs, rhs| lhs / rhs }
}

stack = []

ARGV[0].split(' ').each do |tok|
  if /^[0-9]*$/ =~ tok
    stack << tok.to_i
  elsif ops.key?(tok.intern)
    die("too few numbers provided") if stack.size < 2
    rhs = stack.pop
    lhs = stack.pop
    die("divide by 0") if tok == '/' && rhs.zero?
    stack << ops[tok.intern].call(lhs, rhs)
  else
    die("unrecognized token '#{tok}'")
  end
end

die("too few operators provided") unless stack.size == 1
puts("result: #{stack.pop}")
