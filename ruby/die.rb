#!/usr/bin/env ruby

require "optparse"

options = { debug: false, verbose: false }

OptionParser.new do |opts|
    opts.banner = "usage: #{__FILE__} [options] [file...]"

    opts.on "-d", "--debug", "enable debug mode" do
        options[:debug] = true
    end

    opts.on "-v", "--verbose", "show verbose output" do
        options[:verbose] = true
    end
end.parse!

exit unless ARGV.size == 1

stage = :count
die = { count: 1, type: 6, mod: 0, score: 0 }
token = ""

puts "[debug] beginning stage count" if options[:debug]

ARGV.shift.each_char do |lexeme|
    if /^[0-9]+$/ === lexeme then
        token += lexeme
    elsif lexeme == "d" and stage == :count then
        puts "[debug] found count: #{token}" if options[:debug]
        puts "[debug] beginning stage type" if options[:debug]

        stage = :type
        die[:count] = token.to_i
        token = ""
    elsif ["+", "-"].include? lexeme and stage == :type then
        if token == "" then
            puts "a die type must be specified"
            exit 1
        end

        puts "[debug] found type: #{token}" if options[:debug]
        puts "[debug] beginning stage modifier" if options[:debug]

        stage = :modifier
        die[:type] = token.to_i
        token = lexeme
    elsif ["+", "-"].include? lexeme and stage == :modifier then
        puts "[debug] found modifier: #{token}" if options[:debug]

        die[:mod] += token.to_i
        token = lexeme
    else
        puts "unknown token #{token + lexeme} while parsing die #{stage.to_s}"
        puts "format is <number of dice> + d + <type of die> + <+|- modifier>"
        puts "ex: 3d4+12-5 or 2d6+7"
        exit 1
    end
end

if token != "" and stage == :modifier then
    puts "[debug] found modifier: #{token}" if options[:debug]
    die[:mod] += token.to_i
end

puts "count: #{die[:count]}, type: #{die[:type]}, modifier: #{die[:mod]}" if options[:verbose]

die[:count].times do |count|
    roll = Random.rand(die[:type]) + 1
    die[:score] += roll

    puts "[debug] roll \##{count + 1}: #{roll}" if options[:debug]
    puts "roll \##{count + 1} is #{roll}" if options[:verbose]
end

puts die[:score] + die[:mod]
