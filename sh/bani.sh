#!/bin/sh

##########
# Config #
##########

name=banish-bot
user=banish-bot
real=./bot.sh
chan=##banish

rm -rf /tmp/banish-pipe
mkfifo /tmp/banish-pipe

match() {
    case $2 in
    $1)
        return 0
        ;;
    *)
        return 1
        ;;
    esac
}

openssl s_client -connect chat.freenode.net:6697 -quiet 2> /dev/null < /tmp/banish-pipe | {
    printf 'NICK %s\r\n' "$name" > /tmp/banish-pipe
    printf 'USER %s * * :%s\r\n' "$user" "$real" > /tmp/banish-pipe

    while IFS= read -r line; do
        line=${line%?}
        from=${line%% *}
        line=${line#* }
        nick=${from%%!*}
        nick=${nick#:}
        from=${from#*@}

        if match '001 *' "$line"; then
            printf 'JOIN %s\r\n' "$chan" > /tmp/banish-pipe
        elif [ "$line" = "JOIN $chan" ] && ! match '*/nchambers' "$from" && [ "$nick" != "$name" ]; then
            printf 'MODE %s +b *!*@%s\r\n' "$chan" "$from" > /tmp/banish-pipe
            printf 'KICK %s %s\r\n' "$chan" "$nick" > /tmp/banish-pipe
        elif [ "$from" = PING ]; then
            printf 'PONG %s\r\n' "$line" > /tmp/banish-pipe
        fi
    done
}
