#lang racket
(require racket/match)

(define interp
  (lambda (exp env)
    (match exp
      ((? symbol?) (env exp))
      ((? number?) exp)
      (`(lambda (,x) ,b) (lambda (a) (interp b (lambda (y) (if (eqv? y x) a (env y))))))
      (`(,rat ,rand) ((interp rat env) (interp rand env))))))

(interp '((lambda (x) x) 5) (lambda () (lambda (y) (error 'interp "unbound var ~s" y)))) ;; => 5
