#!/usr/bin/perl

# prtscn.pl - scan server for open ports

use strict;
use warnings;

use IO::Socket;
use Getopt::Long;

my $verbosity = undef;
my $use_first_port = undef;

GetOptions('v' => \$verbosity, 'u' => \$use_first_port);

sub scan {
    my $server = shift;
    my $start_port = shift;
    my $end_port = shift;
    my @open_ports = ();

    while($start_port <= $end_port) {
        if($verbosity) {
            print "scanning port $start_port => ";
        }

        my $sock = IO::Socket::INET->new(PeerHost => $server, PeerPort => $start_port, Timeout => 0.4);

        if($verbosity) {
            if($sock) {
                print "open\n";
            } else {
                print "closed\n";
            }
        }

        if($sock) {
            push @open_ports, $start_port;

            if($use_first_port) {
                return @open_ports;
            }
        }

        $start_port++;
    }

    return @open_ports;
}

sub pretty_print {
    my @open = @_;
    my @closed = ();
    my @current_block = ();

    foreach my $elem (@open) {
        if(@current_block && $elem == $current_block[-1]+1) {
            push @current_block, $elem;
        } else {
            if(scalar @current_block > 1) {
                push @closed, "$current_block[0]-$current_block[-1]";
            } elsif(scalar @current_block == 1) {
                push @closed, @current_block;
            }

            @current_block = ($elem,);
        }
    }

    if(@current_block) {
        if(scalar @current_block > 1) {
            push @closed, "$current_block[0]-$current_block[-1]";
        } elsif(scalar @current_block == 1) {
            push @closed, @current_block;
        }
    }

    return join ", ", @closed;
}

my $server = shift || die "err: need server.\n";
my $range_list = shift || "1-65536";
my @ranges = split /,/, $range_list || die "err: need range list.\n";
my @open_ports = ();

foreach my $range (@ranges) {
    my ($start, $end) = (undef, undef);

    if($range =~ /-/) {
        ($start, $end) = split /-/, $range;
    } else {
        ($start, $end) = ($range, $range);
    }

    push @open_ports, scan($server, $start, $end); 
}

if(scalar @open_ports != 0) {
    print "open port(s): ${\pretty_print sort { $a <=> $b } @open_ports}\n";
} else {
    print "no ports listed are open.\n";
}
