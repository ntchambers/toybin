#!/bin/sh

num=${1:-42} str=

while [ "$num" -gt 0 ]; do
    str=$((num % 2))$str
    num=$((num / 2))
done

size=$(( ${#str} % 8 ))

while [ "$size" -gt 0 ]; do
    str=0$str
    size=$(( ${#str} % 8 ))
done

printf '%s\n' "$str"
