#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if(argc == 2) {
        int num = atoi(argv[1]);
        int is_prime = 1;

        if(num < 3) {
            is_prime = 1;
        } else if(num % 2 == 0) {
            is_prime = 0;
        } else {
            for(int count = 3; count <= num / 2 + 1; count += 2) {
                if(num % count == 0) {
                    is_prime = 0;
                    break;
                }
            }
        }

        printf("%d is %s.\n", num, is_prime ? "prime" : "not prime");
    }

    return 0;
}
