#!/usr/bin/env ruby

# a very sparkly lisp

module Raspberry
  class Engine
    attr_accessor :env

    def initialize(**opts)
      @env = { }
      @shared = opts[:shared_env] || true
    end

    public

    def add_builtin(name, &block)
      @env[name] = Token.new :lambda, Type::Lambda.new([], block, true)
    end

    def eval(**opts)
      iter = if opts[:token_stream] then
        opts[:token_stream].each
      elsif opts[:iter] then
        opts[:iter]
      elsif opts[:stream] then
        opts[:stream].each
      elsif opts[:file] then
        stream = TokenStream.new **opts, file: opts[:file]
        stream.each
      else
        stream = TokenStream.new **opts, file: File.new(**opts)
        stream.each
      end

      result = nil
      count = 0

      loop do
        token = iter.peek

        result = case token.type
        when :begin
          self.eval_expression iter: iter
        when :name
          self.eval_name iter: iter
        when :list_begin
          self.eval_list iter: iter
        when :string
          self.eval_string iter: iter
        when :integer
          self.eval_integer iter: iter
        when :float
          self.eval_float iter: iter
        else
          raise Error::UnexpectedToken.new(token)
        end

        count += 1 if opts[:max]
        break if opts[:max] and count == opts[:max]
      end

      @env = Environment.new unless @shared
      result
    end

    protected

    def eval_call(func, **opts)
      iter = opts[:iter]

      if func.val.builtin then
        func.val.body.call iter
      else
        args = []

        loop do
          token = iter.peek
          break if token.type == :end
          args << self.eval(iter: iter, max: 1)
        end

        old_env = @env
        @env = @env.clone

        if func.val.args.size > args.size then
          raise Error::ArgumentError.new(func.val.args.size, args.size)
        end

        @env.update func.val.args.map { |arg| arg.to_s }.zip(args).to_h
        result = self.eval token_stream: func.val.body
        @env = old_env
        iter.next
        result
      end
    end

    def eval_expression(**opts)
      iter = opts[:iter]
      iter.next
      token = iter.peek

      if token.type == :name and token.val == "lambda" then
        self.eval_lambda iter: iter
      else
        head = self.eval iter: iter, max: 1

        if head.type == :lambda then
          self.eval_call head, iter: iter
        else
          result = head

          loop do
            token = iter.peek
            break if token.type == :end
            result = self.eval iter: iter, max: 1
          end

          iter.next
          result
        end
      end
    end

    def eval_float(**opts)
      iter = opts[:iter]
      iter.next
    end

    def eval_integer(**opts)
      iter = opts[:iter]
      iter.next
    end

    def eval_lambda(**opts)
      iter = opts[:iter]
      iter.next
      token = iter.next
      args = []
      body = []

      if token.type == :begin then
        loop do
          token = iter.next
          break if token.type == :end
          raise Error::UnexpectedToken.new(token) unless token.type == :name
          args << token
        end
      else
        raise Error::UnexpectedToken.new(token) unless token.type == :name
        args << token
      end

      depth = 0

      loop do
        token = iter.next
        break if token.type == :end and depth == 0

        if token.type == :begin then
          depth += 1
        elsif token.type == :end then
          depth -= 1
        end

        body << token
      end

      Token.new :lambda, Type::Lambda.new(args, body)
    end

    def eval_list(**opts)
      items = []
      iter = opts[:iter]
      iter.next

      loop do
        token = iter.peek
        break if token.type == :list_end
        items << self.eval(**opts, iter: iter, max: 1)
      end

      iter.next
      Token.new :list, Type::List.new(*items)
    end

    def eval_name(**opts)
      iter = opts[:iter]
      sym = iter.next

      if @env.key? sym.to_s then
        @env[sym.to_s]
      else
        raise Error::UndefinedSymbol.new(sym)
      end
    end

    def eval_string(**opts)
      iter = opts[:iter]
      iter.next
    end
  end

  Environment = Hash

  module Error
    class BaseEngineError < StandardError
      attr_reader :message

      def initialize(msg)
        @message = msg
      end
    end

    class ArgumentError < BaseEngineError
      def initialize(expected, actual)
        @message = "expected #{expected} arguments, got #{actual} arguments"
      end
    end

    class UnexpectedToken < BaseEngineError
      def initialize(token)
        @message = "unexpected token #{token.to_s}"
      end
    end

    class UndefinedSymbol < BaseEngineError
      def initialize(sym)
        @message = "undefined symbol #{sym.to_s}"
      end
    end
  end

  class File
    attr_reader :name, :prog, :size
    attr_accessor :line, :col, :word_start, :pos

    def initialize(**opts)
      @name = opts[:name]
      @prog = opts[:prog]
      @size = @prog.size
      @line = 1
      @col = 0
      @word_start = 0
      @pos = 0
    end
  end

  class Token
    attr_reader :type, :val

    def initialize(type, val)
      @type = type
      @val = val
    end

    def inspect
      "#{self.class.to_s}<#{@type}: #{@val.inspect}>"
    end

    def to_s
        begin
          @val.to_s
        rescue
          "#{@val.class.to_s} not printable as Raspberry String"
        end
    end
  end

  class TokenStream
    def initialize(**opts)
      @file = opts[:file]
    end

    public

    def each(&block)
      if block_given? then
        tokens = []

        loop do
          token = self.get
          tokens << token
          yield token
        end

        tokens
      else
        Enumerator.new do |enumerator|
          loop do
            token = self.get
            enumerator << token
          end
        end
      end
    end

    protected

    def get
      in_comment = false
      in_str = false
      token = Token.new :eof, nil
      value = ""
      advance = true

      while @file.pos < @file.size and token.type == :eof do
        if @file.pos == @file.size then
          raise StopIteration
        end

        lexeme = @file.prog[@file.pos]

        if in_comment then
          if lexeme == "\n" then
            in_comment = false
          end
        elsif in_str and lexeme == "\"" then
          in_str = false
          token = Token.new :string, Type::String.new(value)
        elsif in_str then
          value += lexeme
        elsif lexeme == "\"" then
          in_str = true
        elsif lexeme == "#" then
          in_comment = true
        elsif /^[ \t\n]$/ === lexeme then
          token = case value
          when /^[0-9]+$/
            Token.new :integer, Type::Integer.new(value.to_i)
          when /^[0-9]+\.[0-9]+$/
            Token.new :float, Type::Float.new(value.to_f)
          when ""
            token
          else
            Token.new :name, value
          end
        elsif lexeme == "(" then
          token = Token.new :begin, "("
        elsif lexeme == ")" then
          token = case value
          when /^[0-9]+$/
            Token.new :integer, Type::Integer.new(value.to_i)
          when /^[0-9]+\.[0-9]+$/
            Token.new :float, Type::Float.new(value.to_f)
          when ""
            Token.new :end, ")"
          else
            Token.new :name, value
          end

          advance = false unless token.type == :end
        elsif lexeme == "[" then
          token = Token.new :list_begin, "["
        elsif lexeme == "]" then
          token = case value
          when /^[0-9]+$/
            Token.new :integer, Type::Integer.new(value.to_i)
          when /^[0-9]+\.[0-9]+$/
            Token.new :float, Type::Float.new(value.to_f)
          when ""
            Token.new :list_end, "]"
          else
            Token.new :name, value
          end

          advance = false unless token.type == :list_end
        else
          value += lexeme
        end

        @file.pos += 1 if advance
      end

      if token.type == :eof then
        raise StopIteration
      else
        token
      end
    end
  end

  module Type
    class Object
      attr_accessor :val

      def initialize(val)
        @val = val
      end

      def inspect
        "#{self.class.to_s}<#{@val.inspect}>"
      end

      def to_s
        @val.to_s
      end
    end

    class Float < Object
    end

    class Integer < Object
    end

    class Lambda < Object
      attr_accessor :args, :body, :builtin

      def initialize(args, body, builtin=false)
        @args = args
        @body = body
        @builtin = builtin
      end

      def inspect
        if @builtin then
          "Lambda<builtin>"
        else
          args = @args.map { |arg| arg.inspect }.join " "
          body = @body.map { |tok| tok.inspect }.join " "
          "#{self.class.to_s}<#{args}>:\n  #{body}"
        end
      end

      def to_s
        if @builtin then
          "Lambda<builtin>"
        else
          args = @args.map { |arg| arg.to_s }.join " "
          "#{self.class.to_s}<#{args}>"
        end
      end
    end

    class List < Object
      def initialize(*val)
        @val = val
      end

      def to_s
        items = @val.map { |item| item.to_s }
        "[#{items.join " "}]"
      end
    end

    class String < Object
      def inspect
        "\"#{@val.to_s}\""
      end

      def to_s
        "#{@val.to_s}"
      end
    end
  end
end

begin
  ARGV.each do |filename|
    prog = File.open filename do |file|
      file.read
    end

    engine = Raspberry::Engine.new

    engine.add_builtin "emit" do |iter| # helper used for debugging
      args = []

      loop do
        token = iter.peek
        break if token.type == :end
        args << engine.eval(iter: iter, max: 1)
      end

      puts args.join(" ")
      iter.next
      args[-1]
    end

    engine.add_builtin "define" do |iter|
      symbol = iter.next.val
      engine.env[symbol] = engine.eval iter: iter, max: 1
      iter.next
      symbol
    end

    engine.add_builtin "env-debug" do |iter|
      iter.next
      p engine.env
      nil
    end

    result = engine.eval name: filename, prog: prog
    puts result.to_s
  end
rescue Raspberry::Error::BaseEngineError => err
  puts "error: #{err.message}"
end
