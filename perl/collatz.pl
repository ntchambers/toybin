#!/usr/bin/perl

use warnings;
use strict;

if(scalar @ARGV == 0) {
    die 'requires an argument';
} elsif($ARGV[0] < 2) {
    die 'argument must be greater than 1';
}

sub collatz {
    my ($value, $steps) = @_;

    if($value == 1) {
        return $steps;
    }

    if($value % 2 == 0) {
        return collatz($value/2, $steps+1);
    } else {
        return collatz($value*3+1, $steps+1);
    }
}

print "${\collatz($ARGV[0], 0)}\n";
