#!/usr/bin/env bash

if [[ -z $1 ]]; then
    printf 'You need a channel dumbass.\n' >&2
    exit 1
fi

printf '/join #%s,0\n' "$(openssl sha1 <<< "$1" | awk '{ print $2 }' | dd count=20 2>/dev/null)"
