#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

unsigned int vfexec(const char *fmt, va_list args) {
    char cmd_str[ARG_MAX + 1] = { 0 };
    vsnprintf(cmd_str, ARG_MAX, fmt, args);

    unsigned int arg_count = 1;
    unsigned int arg_size = 25; /* 25 is used for no particular reason */
    char *arg = strtok(cmd_str, " \t\n");
    char **arguments = malloc(sizeof(char *) * 25);
    arguments[0] = arg;

    while((arg = strtok(NULL, " \t\n")) != NULL) {
        if(arg_count == arg_size) {
            arg_size += 5; /* again, no particular reason */
            void *tmp = realloc(arguments, sizeof(char *) * arg_size);

            if(tmp == NULL) {
                int cached = errno;
                fprintf(stderr, "fexec: realloc: %s\n", strerror(errno));
                return cached;
            }

            arguments = tmp;
        }

        arguments[arg_count++] = arg;
    }

    arguments[arg_count] = NULL;
    pid_t pid = fork();

    if(pid > 0) {
        int result = 0;
        wait(&result);
        return result;
    } else if(pid < 0) {
        int cached = errno;
        fprintf(stderr, "fexec: fork: %s\n", strerror(errno));
        return cached;
    } else {
        int result = execvp((const char *) arguments[0], arguments);

        if(result < 0) {
            int cached = errno;
            fprintf(stderr, "fexec: execvp: %s\n", strerror(errno));
            return cached;
        }
    }

    return 0;
}

unsigned int fexec(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    unsigned int result = vfexec(fmt, args);
    va_end(args);
    return result;
}

int main() {
    fexec("echo %s", "Hell, world!");
    fexec("find /Users/nchambers -name .*rc -print"); /* not a shell so no quoting necessary */
    return 0;
}
