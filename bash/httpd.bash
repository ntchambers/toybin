#!/bin/bash

if (( $# != 1 )); then
    printf 'usage: %s [config file]\n' "$0" >&2
    exit 1
fi

if [[ ! -f $1 ]]; then
    printf 'could not open %s\n' "$1" >&2
    exit 1
fi

. "$1"

for name in host port root access_log; do
    if [[ ! -v $name ]]; then
        printf 'no configuration value for %s found\n' "$name"
    fi
done

mkfifo "/tmp/http_pipe.$$"

if [[ $access_log != '-' ]]; then
    exec 1<>"$access_log"
fi

tail -n +1 -f "/tmp/http_pipe.$$" | nc -vkl "$host" "$port" 2>&1 | while read -r line; do
    case "${line%% *}" in
    'Listening')
        printf 'Surf'\''s up on http://%s:%d/!\n' "$host" "$port"
    ;;

    'Connection')
        client_ip=${line#*[} client_ip=${client_ip%%]*}
    ;;

    'GET')
        read -r file _ <<< "${line#* }"

        if [[ $file = '/' ]]; then
            file=index.html
        fi

        if [[ -f $root/$file ]]; then
            printf '%s is being served http://%s:%d/%s\n' "$client_ip" "$host" "$port" "$file"
            printf 'HTTP/1.1 200 OK\r\n' > "/tmp/http_pipe.$$"
            printf 'Date: %s\r\n' "$(date)" > "/tmp/http_pipe.$$"
            printf 'Server: httpd.bash\r\n' > "/tmp/http_pipe.$$"
            printf 'Content-Length: %d\r\n' "$(wc "$root/$file" | awk '{ print $3; }')" > "/tmp/http_pipe.$$"
            printf 'Content-Type: text/html\r\n' > "/tmp/http_pipe.$$"
            printf '\r\n' > "/tmp/http_pipe.$$"
            cat "$root/$file" > "/tmp/http_pipe.$$"
        else
            printf '%s attempted to get http://%s:%d/%s\n' "$client_ip" "$host" "$port" "${file:1}"
            printf 'HTTP/1.1 404 Not Found\r\n' > "/tmp/http_pipe.$$"
            printf 'Date: %s\r\n' "$(date)" > "/tmp/http_pipe.$$"
            printf 'Server: httpd.bash\r\n' > "/tmp/http_pipe.$$"
            printf 'Content-Length: %d\r\n' "$(wc "$root/404.html" | awk '{ print $3; }')" > "/tmp/http_pipe.$$"
            printf 'Content-Type: text/html\r\n' > "/tmp/http_pipe.$$"
            printf '\r\n' > "/tmp/http_pipe.$$"
            cat "$root/404.html" > "/tmp/http_pipe.$$"
        fi
    ;;

    *)
    ;;
    esac
done
