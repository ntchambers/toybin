#!/usr/bin/env gawk

# this isn't standard POSIX awk, but it does run under gawk.

function send(msg) {
    printf "%s\r\n", msg |& sock
    printf ">>> %s\n", msg
}

function nick(new_nick) {
    send("NICK "new_nick)
}

function user(nick) {
    send("USER "nick" * * :"nick)
}

function join(chan) {
    send("JOIN "chan)
}

function privmsg(target, msg) {
    send("PRIVMSG "target" :"msg)
}

BEGIN {
    serv = "chat.freenode.net"
    port = "6667"
    name = "awkbot"
    sock = "/inet/tcp/0/"serv"/"port
    chan = "#botters-test"

    nick(name)
    user(name)

    while((sock |& getline) > 0) {
        print $0

        if($0 ~ /^:awkbot MODE awkbot :\+i/) {
            break
        }
    }

    join(chan)
    privmsg("#botters-test", "Hello, world!")

    while((sock |& getline) > 0) {
        print $0

        if($1 == "PING") {
            send("PONG "$2)
        } else if($2 == "PRIVMSG") {
            msg = ""

            for(i = 4; i <= NF; i++) {
                msg = msg$i
            }

            gsub(/\r/, "", msg)
            other_nick = substr($1, 2, index($1, "!") - 2)

            if(msg == ":hello!") {
                privmsg($3, "Hello, "other_nick"!")
            }
        }
    }
}

END {
    close(sock)
}
