/*
 * Base64 encoder/decoder
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

const char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char *encode(const char *str) {
    size_t strsize = strlen(str);
    size_t count = 0;
    size_t encodedsize = (size_t) ceil(strsize / 3.0) * 4;
    size_t encodedpos = 0;
    char *encoded = malloc(encodedsize);

    for(; count + 2 < strsize; count += 3) {
        encoded[encodedpos++] = table[(str[count] >> 2) & 0x3F];
        encoded[encodedpos++] = table[((str[count] << 4) | (str[count + 1] >> 4)) & 0x3F];
        encoded[encodedpos++] = table[((str[count + 1] << 2) | (str[count + 2] >> 6)) & 0x3F];
        encoded[encodedpos++] = table[str[count + 2] & 0x3F];
    }

    if(strsize - count == 1) {
        encoded[encodedpos++] = table[(str[count] >> 2) & 0x3F];
        encoded[encodedpos++] = table[((str[count] << 4) | (1 >> 4)) & 0x3F];
        encoded[encodedpos++] = '=';
        encoded[encodedpos++] = '=';
    } else if(strsize - count == 2) {
        encoded[encodedpos++] = table[(str[count] >> 2) & 0x3F];
        encoded[encodedpos++] = table[((str[count] << 4) | (str[count + 1] >> 4)) & 0x3F];
        encoded[encodedpos++] = table[((str[count + 1] << 2) | (1 >> 6)) & 0x3F];
        encoded[encodedpos++] = '=';
    }

    return encoded;
}

const int detable[] = {
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64,
    64,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64,
    64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};

char *decode(const char *str) {
    size_t strsize = strlen(str);
    size_t count = 0;
    size_t decodedsize = (size_t) strsize / 4 * 3;
    size_t decodedpos = 0;
    char *decoded = malloc(decodedsize);

    for(; count + 3 < strsize; count += 4) {
        if(str[count + 3] == '=') {
            break;
        }

        decoded[decodedpos++] = (detable[(int) str[count]] << 2) | (detable[(int) str[count + 1]] >> 4);
        decoded[decodedpos++] = (detable[(int) str[count + 1]] << 4) | (detable[(int) str[count + 2]] >> 2);
        decoded[decodedpos++] = (detable[(int) str[count + 2]] << 6) | detable[(int) str[count + 3]];
    }

    if(count < strsize) {
        if(str[count + 2] == '=' && str[count + 3] == '=') {
            decoded[decodedpos++] = (detable[(int) str[count]] << 2) | (detable[(int) str[count + 1]] >> 4);
        } else if(str[count + 3] == '=') {
            decoded[decodedpos++] = (detable[(int) str[count]] << 2) | (detable[(int) str[count + 1]] >> 4);
            decoded[decodedpos++] = (detable[(int) str[count + 1]] << 4) | (detable[(int) str[count + 2]] >> 2);
        }
    }

    decoded[decodedpos] = '\0';
    return decoded;
}

int main(int argc, char **argv) {
    if(argc == 3 && strcmp(argv[1], "-d") == 0) {
        printf("%s\n", decode(argv[2]));
    } else if(argc == 2) {
        printf("%s\n", encode(argv[1]));
    }

    return 0;
}
