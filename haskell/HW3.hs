{-# OPTIONS -W #-}

module HW3 where

import Data.Function (on)

-- types for all interpreters
type Name = String

data Term = Int Integer
          | Sub Term Term
          | Less Term Term
          | If Term Term Term
          | Var Name
          | Let Name Term Term
          | Lam Name Term
          | App Term Term
    deriving (Show)

-- value type for original interpreter
data Value = N { nOf :: Double }
           | B { bOf :: Bool }
           | F { fOf :: (Value -> Value) }

instance Show Value where
  show (N x) = "N " ++ show x
  show (B x) = "B " ++ show x
  show (F _) = "<#procedure>"

-- defunctionalized implementation of closures
data FuncD = Clos { closName :: Name
                  , closBody :: Term
                  , closEnv :: Env ValueD
                  }
           deriving (Show)

data ValueD = N_d { nOf_d :: Double }
            | B_d { bOf_d :: Bool }
            | F_d { fOf_d :: FuncD }
            deriving (Show)

-- nothing changes from above, just renamed for question
data FuncC = Clos_c { closName_c :: Name
                    , closBody_c :: Term
                    , closEnv_c :: Env ValueC
                    }
           deriving (Show)

data ValueC = N_c { nOf_c :: Double }
            | B_c { bOf_c :: Bool }
            | F_c { fOf_c :: FuncC }
            deriving (Show)

-- environment reprentation using polymorphic types
type Env a = [(Name,a)]

empty :: Env a
empty = []

extend :: Name -> a -> Env a -> Env a
extend n v e = (n,v):e

lookupEnv :: Env a -> Name -> a
lookupEnv [] n = error ("unbounded variable: " ++ n)
lookupEnv (e:es) n = if n == fst e
                     then snd e
                     else lookupEnv es n

valueOf :: Env Value -> Term -> Value
valueOf _ (Int i) = N (fromIntegral i)
valueOf env (Var n) = lookupEnv env n
valueOf env (Sub x y) = N (calcValue env (-) x y)
valueOf env (Less x y) = B (calcValue env (<) x y)
valueOf env (If x y z) = if bOf (valueOf env x) then valueOf env y else valueOf env z
valueOf env (Let n e b) = valueOf (extend n (valueOf env e) env) b
valueOf env (Lam n b) = F (\a -> (valueOf (extend n a env) b))
valueOf env (App rat rand) = ((fOf $ valueOf env rat) (valueOf env rand))

calcValue :: Env Value -> (Double -> Double -> a) -> Term -> Term -> a
calcValue env op = on op (nOf . (valueOf env))

-- defunctionalized interpreter

valueOf_d :: Env ValueD -> Term -> ValueD
valueOf_d _ (Int i) = N_d (fromIntegral i)
valueOf_d env (Var n) = lookupEnv env n
valueOf_d env (Sub x y) = N_d (calcValue_d env (-) x y)
valueOf_d env (Less x y) = B_d  (calcValue_d env (<) x y)
valueOf_d env (If x y z) = if bOf_d (valueOf_d env x) then valueOf_d env y else valueOf_d env z
valueOf_d env (Let n e b) = valueOf_d (extend n (valueOf_d env e) env) b
valueOf_d env (Lam n b) = F_d $ Clos n b env
valueOf_d env (App rat rand) =
  let closure = fOf_d $ valueOf_d env rat
  in valueOf_d (extend (closName closure) (valueOf_d env rand) $ closEnv closure) $ closBody closure
     
calcValue_d :: Env ValueD -> (Double -> Double -> a) -> Term -> Term -> a
calcValue_d env op = on op (nOf_d . (valueOf_d env))



-- CPS interpreter

valueOf_c :: Env ValueC -> Term -> (ValueC -> ValueC) -> ValueC
valueOf_c _ (Int i) k = k $ N_c (fromIntegral i)
valueOf_c env (Var n) k = k $ lookupEnv env n
valueOf_c env (Sub x y) k = calcValue_c env (-) x y (\r -> k (N_c r))
valueOf_c env (Less x y) k = calcValue_c env (<) x y (\r -> k (B_c r))
valueOf_c env (If x y z) k =
  valueOf_c env x (\r ->
                    if bOf_c r
                    then valueOf_c env y k
                    else valueOf_c env z k)
valueOf_c env (Let n e b) k = valueOf_c env e (\r ->
                                                valueOf_c (extend n r env) b k)
valueOf_c env (Lam n b) k = k $ F_c $ Clos_c n b env
valueOf_c env (App rat rand) k =
  valueOf_c env rat (\r ->
                      valueOf_c env rand (\r1 ->
                                           applyF_c (fOf_c r) r1 k))

applyF_c :: FuncC -> ValueC -> (ValueC -> ValueC) -> ValueC
applyF_c f a k = valueOf_c (extend (closName_c f) a (closEnv_c f)) (closBody_c f) k

calcValue_c :: Env ValueC -> (Double -> Double -> a) -> Term -> Term -> (a -> ValueC) -> ValueC
calcValue_c env op x y k = valueOf_c env x (\r ->
                                             valueOf_c env y (\s ->
                                                               k (op (nOf_c r) (nOf_c s))))

-- Make the interpreter independent of how continuations (and operators)

add :: Term -> Term -> Term
add x y = Sub x (Sub (Int 0) y)

-- we can moresimply derive show for our functions and actually be able
-- to show meaningful information about closures at any point.
