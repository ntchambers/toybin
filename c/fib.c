#include <stdio.h>

void fib(long lhs, long rhs, int count, int max) {
    if(count == max) {
        return;
    }

    printf("%d: %ld\n", count + 1, lhs + rhs);
    fib(rhs, lhs + rhs, count + 1, max);
}

int main() {
    fib(0, 1, 0, 100);
    return 0;
}
