#include <stdio.h>
#include <stdlib.h>

size_t digits(int num) {
    num *= (num < 0) ? -1 : 1;

    if(num < 10) {
        return 1;
    } else if(num < 100) {
        return 2;
    } else if(num < 1000) {
        return 3;
    } else if(num < 10000) {
        return 4;
    } else if(num < 100000) {
        return 5;
    } else {
        size_t count = 1;

        while(num > 10) {
            num /= 10;
            ++count;
        }

        return count;
    }
}

int main(int argc, char **argv) {
    if(argc == 2) {
        size_t size = atoi(argv[1]);
        printf("%zu\n", digits(size));
    }

    return 0;
}
