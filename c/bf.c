/*
 * Brainfuck Interpreter
 * Should probably be rewritten
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define TAPE_SIZE 1024

char *process(char *prog, int *tape, int tape_size, int *tape_loc, bool skip) {
    for(char *instruction = prog; *instruction; ++instruction) {
        if(*instruction == '+' && !skip) {
            tape[*tape_loc] += 1;

            if(tape[*tape_loc] == tape_size + 1) {
                tape[*tape_loc] = 0;
            }
        } else if(*instruction == '-' && !skip) {
            tape[*tape_loc] -= 1;

            if(tape[*tape_loc] == -1) {
                tape[*tape_loc] = tape_size;
            }
        } else if(*instruction == '>' && !skip) {
            *tape_loc += 1;

            if(*tape_loc == tape_size) {
                *tape_loc = 0;
            }
        } else if(*instruction == '<' && !skip) {
            *tape_loc -= 1;

            if(*tape_loc == -1) {
                *tape_loc = tape_size - 1;
            }
        } else if(*instruction == '.' && !skip) {
            fputc(tape[*tape_loc], stdout);
        } else if(*instruction == ',' && !skip) {
            tape[*tape_loc] = fgetc(stdin);
        } else if(*instruction == '[') {
            if(skip || tape[*tape_loc] == 0) {
                instruction = process(instruction + 1, tape, tape_size, tape_loc, true);
            } else {
                char *loop_end = process(instruction + 1, tape, tape_size, tape_loc, false);

                if(tape[*tape_loc] == 0) {
                    instruction = loop_end;
                } else {
                    instruction -= 1;
                }
            }
        } else if(*instruction == ']') {
            return instruction;
        }
    }

    return NULL;
}

int main(int argc, char **argv) {
    if(argc == 2) {
        int tape[TAPE_SIZE] = { 0 };
        int tape_loc = 0;
        process(argv[1], tape, TAPE_SIZE, &tape_loc, false);
        printf("\n");
    }

    return 0;
}
