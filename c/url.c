#include <stdlib.h>
#include <string.h>

char *proto_from_url(char *url) {
    int position = 0;

    for(char *iter = url; *iter; ++iter, ++position) {
        if(*iter == ':' && strncmp(iter, "://", 3) == 0) {
            char *proto = malloc(position);
            strncpy(proto, url, position);
            return proto;
        } else if(*iter == '.' || *iter == '/' || *iter == ':') {
            break;
        }
    }

    if(strncmp(url, "mail", 4) == 0) {
        return "smtp";
    } else {
        return "http";
    }
}

char *host_from_url(char *url) {
    int url_len = strlen(url);
    int position = 0;
    int start_position = 0;
    int end_position = 0;
    int found_start = 0;

    for(char *iter = url; *iter; ++iter, ++position) {
        if(*iter == ':' && url_len - start_position > 3 && strncmp(iter, "://", 3) == 0) {
            found_start = 1;
            position += 3;
            start_position = position;
            iter += 3;
        } else if(*iter == '.' && found_start == 0) {
            found_start = 1;
        } else if(*iter == '/' || *iter == ':') {
            end_position = position - 1;
            break;
        }
    }

    if(end_position == 0) {
        end_position = position;
    }

    char *host = malloc(end_position - start_position);
    strncpy(host, url + start_position, end_position - start_position + 1);
    return host;
}

#include <stdio.h>

int main(int argc, char **argv) {
    if(argc == 2) {
        printf("protocol: %s\n", proto_from_url(argv[1]));
        printf("host: %s\n", host_from_url(argv[1]));
    }

    return 0;
}
