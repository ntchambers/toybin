#!/usr/bin/env bash

if [[ $debug = true ]]; then
    set -x
fi

# I've also written SASL-plain authentication, but not in pure bash
# It was mostly in pure bash, but had to call out to base64(1)
# If you can write a base64 implemenation in pure bash, then the following
# code can be used to do SASL-plain authentication:

#send "CAP REQ :sasl"

#while recv line; do
#    read -ra words <<< "$line"
#
#    if [[ ${words[@]:1} = 'CAP * ACK :sasl' ]]; then
#        send "AUTHENTICATE PLAIN"
#    elif [[ $line = 'AUTHENTICATE +' ]]; then
#        send "AUTHENTICATE $(printf '%s\0%s\0%s' "$username" "$username" "$password" | base64)"
#    elif (( ${#words} > 1 )) && [[ ${words[1]} = 903 ]]; then
#        send "CAP END"
#        break
#    fi
#done

# You would just need to define username and password and move it to
# right above the registration loop.

host=irc.freenode.net
port=6667
nick=binbashbot
user=binbashbot
channels="#botters-test"

exec 3<>"/dev/tcp/$host/$port"

recv() {
    if [[ $1 ]]; then
        read -r "$1" <&3;
        printf -v "$1" '%s' "${!1%$'\r'}"
        printf '<<< %s\n' "${!1}"
    fi
}

send() {
    if [[ $1 ]]; then
        printf '%s\r\n' "$1" >&3
        printf '>>> %s\n' "$1"
    fi
}

nick() {
    if [[ $1 ]]; then
        send "NICK $1"
    fi
}

user() {
    if [[ $1 ]]; then
        send "USER $1 * * :$1"
    fi
}

pong() {
    if [[ $1 ]]; then
        send "PONG $1"
    fi
}

join() {
    if [[ $1 ]]; then
        send "JOIN $1"
    fi
}

part() {
    if [[ $1 ]]; then
        send "PART $1"
    fi
}

privmsg() {
    if [[ $1 ]] && [[ $2 ]]; then
        send "PRIVMSG $1 :$2"
    fi
}

quit() {
    if [[ $1 ]]; then
        send "QUIT :$1"
    else
        send "QUIT"
    fi
}

nick "$nick"
user "$user"

while recv line; do
    read -ra words <<< "$line"

    if [[ ${words[0]} = PING ]]; then
        pong "${words[1]}"
    elif (( ${#words} > 1 )) && [[ ${words[1]} = 376 ]]; then
        break;
    fi
done

join "$channels"

while recv line; do
    read -ra words <<< "$line"

    if [[ ${words[0]} = PING ]]; then
        pong "${words[1]}"
    elif (( ${#words} > 3 )) && [[ ${words[1]} = PRIVMSG ]]; then
        from=${words[0]#:} from=${from%%!*}
        target=${words[2]} directive=${words[3]#:}
        msg=${words[@]:4}
        read -ra args <<< "${words[@]:4}"

        if [[ $directive = '!hi' ]]; then
            privmsg "$target" "Hello, $from!"
        elif [[ $directive = '!join' ]] && (( ${#args[@]} > 0 )); then
            join "${args[0]}"
        elif [[ $directive = '!part' ]] && (( ${#args[@]} > 0 )); then
            part "${args[0]}"
        elif [[ $directive = '!quit' ]]; then
            quit "${args[@]}"
        fi
    fi
done
