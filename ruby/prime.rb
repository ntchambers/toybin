#!/usr/bin/env ruby

return unless ARGV.size == 1
return unless ARGV[0] =~ /^[0-9]+$/

num = ARGV[0].to_i
prime = true

if num < 3
  prime = true
elsif (num % 2).zero?
  prime = false
else
  count = 3

  while count <= num / 2 + 1
    if (num % count).zero?
      prime = false
      break
    end

    count += 2
  end
end

puts "#{num} is #{prime ? "prime" : "not prime"}"
