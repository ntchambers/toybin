#!/usr/bin/env awk

{
    count = 0

    for(i = 1; i <= NF; i++) {
        if($i ~ /^[0-9]+$/) {
            stack[count] = $i
            count++
        } else if($i == "+" || $i == "-" || $i == "*" || $i == "/") {
            if(count < 2) {
                print "error: the user has not input sufficient values in the expression."
                exit 1
            }

            rhs = stack[count - 1]
            lhs = stack[count - 2]
            count -= 1

            if($i == "+") {
                stack[count - 1] = lhs + rhs
            } else if($i == "-") {
                stack[count - 1] = lhs - rhs
            } else if($i == "*") {
                stack[count - 1] = lhs * rhs
            } else if($i == "/") {
                if(rhs == 0) {
                    print "error: attempted division by zero."
                    exit 1
                }

                stack[count - 1] = lhs / rhs
            }
        } else {
            printf("error: unrecognized token %s.\n", $i)
            exit 1
        }
    }

    if(count == 1) {
        print stack[0]
    } else {
        print "error: the user input has too many values."
    }
}
