#include <stdio.h>

int main() {
    int doors[100] = { 0 };

    for(int pass = 0; pass < 100; ++pass) {
        for(int door = 0; door < 100; ++door) {
            if((door + 1) % (pass + 1) == 0) {
                doors[door] = !doors[door];
            }
        }
    }

    for(int door = 0; door < 100; ++door) {
        if(doors[door]) {
            printf("door %d is open\n", door + 1);
        }
    }

    return 0;
}
