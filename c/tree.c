#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if(argc == 2) {
        int count = atoi(argv[1]);
        int stars = 1;

        for(; count > 0; --count) {
            for(int space = 0; space < count - 1; ++space) {
                fputc(' ', stdout);
            }

            for(int star = 0; star < stars; ++star) {
                fputc('*', stdout);
            }

            fputc('\n', stdout);
            stars += 2;
        }
    }

    return 0;
}
