#!/usr/bin/perl

use warnings;
use strict;

use Env '$IFS';
use Getopt::Long;

my @stuff = ();
my $a_seperator_i_guess = " ";
my $some_random_elem_thing = 0;

GetOptions('r' => \$some_random_elem_thing);

if(defined $IFS) {
    $a_seperator_i_guess = $IFS;
}

if(scalar @ARGV == 1) {
    @stuff = split /$a_seperator_i_guess/, $ARGV[0];
} else {
    @stuff = @ARGV;
}

for(my $some_iterative_fucking_num = 0; $some_iterative_fucking_num < @stuff; $some_iterative_fucking_num++) {
    my $some_random_fucking_num = int(rand scalar @stuff);
    ($stuff[$some_iterative_fucking_num], $stuff[$some_random_fucking_num]) = ($stuff[$some_random_fucking_num], $stuff[$some_iterative_fucking_num]);
}

if($some_random_elem_thing == 0) {
    print "${\join $a_seperator_i_guess, @stuff}\n";
} elsif($some_random_elem_thing == 1) {
    print "${\$stuff[rand scalar @stuff]}\n";
}
