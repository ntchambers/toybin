#!/usr/bin/env bash

number_stack=( )

while (( $# > 0 )); do
    if [[ $1 =~ [0-9]+ ]]; then
        number_stack+=( $1 )
    else
        if (( ${#number_stack[@]} < 1 )); then
            printf 'not enough numbers\n' >&2
            exit 1
        fi

        size=${#number_stack[@]}
        (( last = size - 1 ))
        (( pen = size - 2 ))

        case $1 in
        +)
            (( res = number_stack[pen] + number_stack[last] ))
            ;;
        -)
            (( res = number_stack[pen] - number_stack[last] ))
            ;;
        \*)
            (( res = number_stack[pen] * number_stack[last] ))
            ;;
        /)
            if (( number_stack[last] == 0 )); then
                printf 'illegal division by 0\n' >&2
                exit 1
            fi

            (( res = number_stack[pen] / number_stack[last] ))
            ;;
        *)
            printf 'unknown token "%s"\n' "$1" >&2
            exit 1
            ;;
        esac

        number_stack[pen]=$res
        unset 'number_stack[last]'
    fi
    shift
done

if (( ${#number_stack[@]} != 1 )); then
    printf 'too many numbers given\n' >&2
    exit 1
else
    printf '%d\n' "$number_stack"
fi
