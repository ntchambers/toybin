#include <stdio.h>

void swap(int *one, int *two) {
    int tmp = *one;
    *one = *two;
    *two = tmp;
}

int partition(int *arr, int low, int high) {
    int pivot = arr[high];
    int i = low - 1;

    for(int j = low; j < high; ++j) {
        if(arr[j] < pivot) {
            ++i;
            swap(&arr[i], &arr[j]);
        }
    }

    swap(&arr[i + 1], &arr[high]);
    return i + 1;
}

void quicksort(int *arr, int low, int high) {
    if(low < high) {
        int pivot = partition(arr, low, high);
        quicksort(arr, low, pivot - 1);
        quicksort(arr, pivot + 1, high);
    }
}

void print(int *arr, int size) {
    fputc('[', stdout);

    for(int count = 0; count < size; ++count) {
        printf("%d%s", arr[count], count < size - 1 ? ", " : "");
    }

    printf("]\n");
}

int main() {
    int arr[] = { 42, 9, 7, 42, 13, 23, 7, 10, 19, 8, 24, 30 };
    print(arr, 12);
    quicksort(arr, 0, 12);
    print(arr, 12);
}
