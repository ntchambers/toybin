#include <ctype.h>
#include <sodium.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef enum {
    AMT, TYPE, MOD
} stage_t;

int str2int(char *begin, char *end) {
    char *substr = calloc(1, end - begin);
    substr = strncpy(substr, begin, end - begin);
    int number = strtol(substr, NULL, 10);
    free(substr);
    return number;
}

int main(int argc, char **argv) {
    char arg = 0;
    bool debug = false;
    bool verbose = false;
    bool silent = false;
    opterr = 0;

    while((arg = getopt(argc, argv, "dvs")) != -1) {
        if(arg == 'd') {
            debug = true;
        } else if(arg == 'v') {
            verbose = true;
        } else if(arg == 's') {
            silent = true;
        } else if(arg == '?') {
            fprintf(stderr, "unknown option '-%c'.\n", optopt);
            return 1;
        } else {
            abort();
        }
    }

    if(argc - optind != 1) {
        fprintf(stderr, "usage: %s [-dvs] roll\n", argv[0]);
        fprintf(stderr, "\n");
        fprintf(stderr, "A note about -s:\n");
        fprintf(stderr, "    Most systems will only take a return\n");
        fprintf(stderr, "    value between 0 and 255, so keep that in\n");
        fprintf(stderr, "    mind.\n");
    } else {
        int amount = 0;
        int sides = 0;
        int modifier = 0;
        char *mark = argv[optind];
        char *tok = mark;
        stage_t stage = AMT;

        if(debug) {
            printf("[DEBUG] Starting stage AMT.\n");
        }

        for(tok = mark; *tok; ++tok) {
            if(*tok == 'd' && stage == AMT) {
                if(debug) {
                    printf("[DEBUG] found the amount marker.\n");
                }

                if(tok == mark) {
                    amount = 1;
                } else {
                    amount = str2int(mark, tok);
                }

                if(debug) {
                    printf("[DEBUG] amount of dice is %d.\n", amount);
                    printf("[DEBUG] starting stage TYPE.\n");
                }

                mark = tok + 1;
                stage = TYPE;
            } else if((*tok == '+' || *tok == '-') && stage == TYPE) {
                if(debug) {
                    printf("[DEBUG] found the type marker.\n");
                }

                if(tok == mark) {
                    fprintf(stderr, "not a valid roll.\n");
                    return 1;
                }

                sides = str2int(mark, tok);
                mark = tok + 1;
                stage = MOD;

                if(debug) {
                    printf("[DEBUG] number of sides is %d.\n", sides);
                    printf("[DEBUG] starting stage MOD.\n");
                }
            } else if((*tok == '+' || *tok == '-') && stage == MOD) {
                if(debug) {
                    printf("[DEBUG] found a modifier marker.\n");
                }

                if(tok == mark) {
                    fprintf(stderr, "not a valid roll.\n");
                    return 1;
                }

                int next_mod = str2int(mark, tok);

                if(debug) {
                    printf("[DEBUG] modifier found: %c%d.\n", *(mark - 1), next_mod);
                }

                if(*(mark - 1) == '+') {
                    modifier += next_mod;
                } else {
                    modifier -= next_mod;
                }

                if(debug) {
                    printf("[DEBUG] total modifier: %d.\n", modifier);
                }

                mark = tok + 1;
            } else if(!isdigit(*tok)) {
                fprintf(stderr, "not a valid roll.\n");
                return 1;
            }
        }

        if(*(tok - 1) == '+' || *(tok - 1) == '-') {
            fprintf(stderr, "not a valid roll.\n");
            return 1;
        } else if(stage == TYPE) {
            if(tok == mark) {
                fprintf(stderr, "not a valid roll.\n");
                return 1;
            }

            sides = str2int(mark, tok);

            if(debug) {
                printf("[DEBUG] number of sides is %d.\n", sides);
            }
        } else if(stage == MOD && tok != mark) {
            int next_mod = str2int(mark, tok);

            if(debug) {
                printf("[DEBUG] modifier found: %c%d.\n", *(mark - 1), next_mod);
            }

            if(*(mark - 1) == '+') {
                modifier += next_mod;
            } else {
                modifier -= next_mod;
            }

            if(debug) {
                printf("[DEBUG] total modifier: %d.\n", modifier);
            }
        }

        if(verbose) {
            printf("amount=%d, sides=%d, modifier=%d\n", amount, sides, modifier);
        }

        int total = 0;

        for(int count = 0; count < amount; ++count) {
            int roll = randombytes_uniform(sides) + 1;
            total += roll;

            if(verbose) {
                printf("Roll %d: %d\n", count + 1, roll);

                if(sides == 20 && roll == 20) {
                    printf("You rolled a natural 20!\n");
                }
            }

            if(debug) {
                printf("[DEBUG] roll %d is %d.\n", count, roll);
                printf("[DEBUG] total is %d.\n", total);
            }
        }

        if(silent) {
            return total + modifier;
        } else {
            printf("%d\n", total + modifier);
        }
    }

    return 0;
}
